import pygame
import pygame.draw
import pygame.event
import pygame.font

class Button:
    FONT = None
    FONT_SIZE = 32
    TEXT_MARGIN = 5
    ROUNDING_RADIUS = 4
    ANTIALIAS = True
    ROUNDED_CORNERS = True
    COLOR_PRESS =   (160, 160, 160)
    COLOR_HOVER =   (180, 180, 180)
    COLOR_NOFOCUS = (200, 200, 200)
    COLOR_TEXT =    (0  ,   0,   0)
    COLOR_BG = None

    def __init__(self, surf: pygame.Surface, text: str, rect: pygame.Rect, onClick, font: pygame.font.Font = None):
        # input validation
        if type(rect) == tuple and len(rect) == 4:
            rect = pygame.Rect(rect)
        elif type(rect) != pygame.Rect:
            raise ValueError(f"Type {type(rect)} is not valid for argument 'rect'")
        if not callable(onClick):
            raise ValueError(f"Type {type(onClick)} is not valid for argument 'onClick', object needs to be callable")
        # init
        self.rect = rect
        self.surf = surf
        self.font = font
        self.onClick = onClick

        self.text = text
        self.state = 0 # 0: idle, 1: hover, 2: press

    def pass_event(self, event: pygame.event.Event):
        if event.type == pygame.MOUSEMOTION:
            if self.rect.collidepoint(event.pos) == True:
                if self.state == 0:
                    self.state = 1
            else:
                self.state = 0
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if self.state == 1:
                self.state = 2
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.state == 2:
                self.onClick()
            

    def draw(self):
        if self.font == None:
            font = pygame.font.SysFont(self.FONT, self.FONT_SIZE)
        else:
            font = self.font
        color = [self.COLOR_NOFOCUS, self.COLOR_HOVER, self.COLOR_PRESS][self.state]
        # background
        pygame.draw.rect(self.surf, color, self.rect, 0, self.ROUNDING_RADIUS if self.ROUNDED_CORNERS else 0)
        # text
        text_surf = font.render(self.text, self.ANTIALIAS, self.COLOR_TEXT, self.COLOR_BG)
        inner_height = self.rect.height - (2 * self.TEXT_MARGIN)
        inner_width = self.rect.width - (2 * self.TEXT_MARGIN)
        text_pos = self.rect.x + self.TEXT_MARGIN + max((inner_width - text_surf.get_width()) / 2, 0), self.rect.y + self.TEXT_MARGIN + max((inner_height - text_surf.get_height()) / 2, 0)
        self.surf.blit(text_surf, text_pos, (0,0, inner_width, inner_height))