import pygame
import pygame.event
import pygame.mouse
import pygame.draw
import pygame.font
import pygame.key
import clipboard

def _only_this_key(key_mask, pressed):
    return key_mask & pressed > 0 and not (key_mask ^ (pow(2,16) - 1)) & pressed > 0

class Textfield:
    FONT = None
    FONT_SIZE = 32
    TEXT_MARGIN = 5
    COLOR_FOCUS =   (200, 200, 200)
    COLOR_NOFOCUS = (225, 225, 225)
    COLOR_TEXT =    (0  ,   0,   0)
    ANTIALIAS = True
    ROUNDED_CORNERS = True
    ROUNDING_RADIUS = 5

    def __init__(self, surf: pygame.Surface, rect: pygame.Rect, font: pygame.font.Font = None):
        # input validation
        if type(surf) != pygame.Surface:
            raise ValueError(f"Type {type(surf)} is not valid for argument 'surf'")
        if font != None and type(font) != pygame.font.Font:
            raise ValueError(f"Type {type(font)} is not valid for argument 'font'")
        if type(rect) == tuple and len(rect) == 4:
            rect = pygame.Rect(rect)
        elif type(rect) != pygame.Rect:
            raise ValueError(f"Type {type(rect)} is not valud for argument 'rect'")
        # init
        self.rect = rect
        self.surf = surf
        self.font = font

        self.text = ""
        self.active = False
    
    def pass_event(self, event: pygame.event.Event):
        if event.type == pygame.KEYDOWN and self.active:
            mmod = event.mod & 53247 # event.mod without NUM, CAPS
            if event.key in [pygame.K_RETURN, pygame.K_KP_ENTER, pygame.K_ESCAPE]:
                self.active = False
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            elif _only_this_key(pygame.KMOD_CTRL, mmod) and event.key == pygame.K_v: # only ctrl is pressed
                txt = clipboard.paste()
                self.text += txt if txt != None else ""
            else:
                self.text += event.unicode
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.active = self.rect.collidepoint(event.pos) == True
    
    def draw(self):
        if self.font == None:
            font = pygame.font.Font(self.FONT, self.FONT_SIZE)
        else:
            font = self.font
        # draw background
        pygame.draw.rect(self.surf, self.COLOR_FOCUS if self.active else self.COLOR_NOFOCUS, self.rect, 0, self.ROUNDING_RADIUS if self.ROUNDED_CORNERS else None)
        # draw text
        text_surf = font.render(self.text, self.ANTIALIAS, self.COLOR_TEXT)
        inner_height = self.rect.height - (self.TEXT_MARGIN * 2)
        inner_width  = self.rect.width  - (self.TEXT_MARGIN * 2)
        text_pos = (self.rect.x + self.TEXT_MARGIN, self.rect.y + self.TEXT_MARGIN + max( (  inner_height - text_surf.get_height()) / 2, 0 ) )
        text_area = max(text_surf.get_width() - inner_width, 0), 0, inner_width, inner_height
        self.surf.blit(text_surf, text_pos, text_area)

class Text:
    FONT = None
    FONT_SIZE = 32
    ANTIALIAS = True
    TEXT_COLOR = (0,0,0)
    BACKGROUND_COLOR = None
    BACKGROUND_ONLY_TEXT = True
    """If True only the background of the text will filled, otherwise the background of the entire rect will be filled."""
    CROP = False
    ALIGNMENT_H = 'L'
    """ can be 'L', 'M', or 'R'; case-insensitive"""
    ALIGNMENT_V = 'T'
    """ can be 'T', 'M', or 'B'; case-insensitive"""

    def __init__(self, surf: pygame.Surface, text: str, rect: pygame.Rect, font: pygame.font.Font = None):
        """Valid types for 'rect': [pygame.Rect, tuple(2), tuple(4)]. If rect is a tuple of length 2, width and height will be 0 (=> alingment locked to top left).

        If font == None: font will be constructed from FONT and FONT_SIZE."""
        if type(surf) != pygame.Surface:
            raise ValueError(f"Type {type(surf)} is not valid for argument 'surf'")
        if font != None and type(font) != pygame.font.Font:
            raise ValueError(f"Type {type(font)} is not valid for argument 'font'")
        if type(rect) == tuple and len(rect) in [4,2]:
            if len(rect) == 2:
                rect = pygame.Rect(rect[0], rect[1], 0, 0)
            else:
                rect = pygame.Rect(rect)
        elif type(rect) != pygame.Rect:
            raise ValueError(f"Type {type(rect)} is not valid for argument 'rect'")
        self.surf = surf
        self.font = font
        self.text = text
        self.rect = rect

    def draw(self):
        # font
        if self.font == None:
            font = pygame.font.SysFont(self.FONT, self.FONT_SIZE)
        else:
            font = self.font
        # render text
        text_surf = font.render(self.text, self.ANTIALIAS, self.TEXT_COLOR, self.BACKGROUND_COLOR)
        text_pos = [self.rect.x, self.rect.y]
        # alignment
        text_crop = [0, 0, self.rect.width, self.rect.height]
        alignment_h = self.ALIGNMENT_H.upper()
        alignment_v = self.ALIGNMENT_V.upper()
        if min(self.rect.width, self.rect.height) <= 0:
            alignment_h = "L"
            alignment_v = "T"
        text_width = min(text_surf.get_width(), text_crop[2]) if self.CROP else text_surf.get_width()
        if alignment_h == "R":
            text_crop[0] = text_surf.get_width() - text_crop[2]
            text_pos[0] += self.rect.width - text_width
        elif alignment_h == "M":
            text_crop[0] = (text_surf.get_width() - text_crop[2]) / 2
            text_pos[0] += (self.rect.width / 2) - (text_width / 2)
        text_height = min(text_surf.get_height(), text_crop[3]) if self.CROP else text_surf.get_height()
        if alignment_v == "B":
            text_crop[1] = text_surf.get_height() - text_crop[3]
            text_pos[1] += self.rect.height - min(text_height, text_crop[3])
        elif alignment_v == "M":
            text_crop[1] = (text_surf.get_height() - text_crop[3]) / 2
            text_pos[1] += (self.rect.height / 2) - (text_height / 2)
        text_crop[0] = max(text_crop[0], 0)
        text_crop[1] = max(text_crop[1], 0)
        # apply text
        if not (self.BACKGROUND_ONLY_TEXT == True):
            pygame.draw.rect(self.surf, self.BACKGROUND_COLOR, self.rect, 0)
        self.surf.blit(text_surf, text_pos, text_crop if self.CROP else None)
